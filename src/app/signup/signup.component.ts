import { api } from './../api';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public signUpForm: FormGroup;
  public hasError:boolean = false;
  public error = '';

  constructor(private router: Router, private fb: FormBuilder, private http:HttpClient) {
    this.signUpForm = this.fb.group({
      email: ['', [Validators.required, Validators.email] ],
      userName: ['', Validators.required],
      password: ['', Validators.required]
   });
   }
  ngOnInit(): void {
    const isToken = localStorage.getItem('token')
    if(isToken){
      this.router.navigate(['/event-details'])
    }
  }

  signUp(){
    let signUpData = this.signUpForm.value;

    this.http.post(api.signUp, signUpData).subscribe((result:any)=>{
      if(result.success){
        localStorage.setItem("token","hgfsdgsdgdstgsdssghfsgfs")
        this.router.navigate(['/event-details'])
      }
      else {
        this.hasError = true;
        this.error = result.response.message;
      }
    })
  }

  inputType(){
    this.hasError = false;
  }

}
