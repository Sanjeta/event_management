import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { api } from './api';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getEvent(){
    return this.http.get(api.getEvent)
  }

  cretaeEvent(data:any){
    return this.http.post(api.cretaeEvent, data)
  }

  getEditData(id:any){
    return this.http.get(api.getEvent+id)
  }

  updateEvent(id:any, data:any){
    return this.http.put(api.updateEvent+id, data.value)
  }

  OnDelete(id:any){
    return this.http.delete(api.updateEvent+id)
  }
}
