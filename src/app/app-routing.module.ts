import { SignupComponent } from './signup/signup.component';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { EventViewComponent } from './event-management/event-view/event-view.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo:'/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'sign-up', component: SignupComponent},
  {path: 'event-details', component:EventViewComponent, canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
