import { api } from './../api';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public logInForm: FormGroup;
  public hasError:boolean = false;
  public error = '';

  constructor(private router: Router, private fb: FormBuilder, private http:HttpClient) {
    this.logInForm = this.fb.group({
      email: ['', [Validators.required, Validators.email] ],
      password: ['', Validators.required]
   });
   }
  ngOnInit(): void {
    const isToken = localStorage.getItem('token')
    if(isToken){
      this.router.navigate(['/event-details'])
    }
  }

  login(){
    let logInData = this.logInForm.value;

    this.http.post(api.login, logInData).subscribe((result:any)=>{
      if(result.success){
        localStorage.setItem("token","hgfsdgsdgdstgsdssghfsgfs")
        this.router.navigate(['/event-details'])
      }
      else {
        this.hasError = true;
        this.error = result.response.message;
      }
    })
  }

  inputType(){
    this.hasError = false;
  }

}
