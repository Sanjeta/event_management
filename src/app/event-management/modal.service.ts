import { ApiService } from 'src/app/api.service';
import { EventAddComponent } from './event-add/event-add.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { api } from '../api';
import { EventEditComponent } from './event-edit/event-edit.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modalRef:any;

  constructor( private api: ApiService,private ngbModal :NgbModal) { }

  openModal(props:any) : Promise<any>{
    this.modalRef = this.ngbModal.open(EventAddComponent,{ centered: true, backdrop : 'static', keyboard : false })
    this.modalRef.componentInstance.setDialogProps(props)

    return this.modalRef.result
  }

  addEvent(addEventForm:FormGroup){
    this.api.cretaeEvent(addEventForm).subscribe(
      (data:any) => {
        this.api.getEvent()
        this.modalRef.close()
      },
      (error) => console.error(error)
    )
  }

  openEditModal(props:any) : Promise<any>{
    this.modalRef = this.ngbModal.open(EventEditComponent,{ centered: true, backdrop : 'static', keyboard : false })
    this.modalRef.componentInstance.setDialogProps(props)

    return this.modalRef.result
  }

  updateEvent(id:string, data:any){
    this.api.updateEvent(id, data).subscribe(
      data => {
        this.api.getEvent()
        this.modalRef.close()
      },
      error => console.error(error)
    )
  }
}
