import { ModalService } from './../modal.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-event-add',
  templateUrl: './event-add.component.html',
  styleUrls: ['./event-add.component.sass']
})
export class EventAddComponent implements OnInit {
  times= [
    '6 AM', '7 AM', '8 AM', '9 AM', '10 AM', '11 AM', '12 AM',
    '1 PM','2 PM','3 PM','4 PM', '5 PM', '6 PM', '7 PM', '8 PM', '9 PM', '10 PM',
  ];
  addEvent:FormGroup;

  constructor(private fb: FormBuilder, private modalservice: ModalService) {
    this.addEvent = this.fb.group({
      eventName: ['', [Validators.required] ],
      Time: [this.times[0], [Validators.required] ],
      Date: ['', [Validators.required] ],
      location: ['', [Validators.required] ],
   });
  }

  ngOnInit(): void {
  }

  onAddEvent(){
    let Data = this.addEvent.value
    Data.Date = new Date(this.addEvent.controls['Date'].value.year, this.addEvent.controls['Date'].value.month, this.addEvent.controls['Date'].value.day)
    if(!this.addEvent.invalid){
     this.modalservice.addEvent(Data)
    }
  }

}
