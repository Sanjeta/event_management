import { ModalService } from './../modal.service';
import { ApiService } from './../../api.service';
import { HttpClient} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


interface Events {
  id?: number;
  eventName: string;
  Date: string;
  Time: string;
  location: string;
}

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.scss']
})
export class EventViewComponent implements OnInit {

  page = 1;
  pageSize:any = 10;
  modal:any;
  countries: Events[] = [];

  constructor(private api: ApiService, private modalService: ModalService) {
  }


  logout(){
    localStorage.removeItem("token")
  }

  ngOnInit(){
   this.getEvents()
  }

  getEvents(){
    this.api.getEvent().subscribe(
      (data:any) => {
        this.countries = data;
        console.log(data)
      },
      (error) => console.log(error)
    )
  }

  open() {
    this.modalService.openModal({}).then(() =>{
    }, () =>{})
  }

  onEdit(id:any){
    this.api.OnDelete(id).subscribe(
      res => {
        this.modalService.openEditModal(res).then(() =>{
        }, () =>{})
      },
      error => alert(error)
    )
  }

  OnDelete(id:any){
    this.api.OnDelete(id).subscribe(
      res => {
        this.getEvents()
      },
      error => alert(error)
    )
  }

}
