import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router:Router){}
  canActivate():boolean {
    let isToken = localStorage.getItem('token')
    if(isToken){
      return true;
    }
    else{
      this.router.navigate(['login'])
      return false
    }
  }

}
